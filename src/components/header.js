import React from 'react'
import logo from '../assets/logo.png';
import './componentsStyle.css'

export const Header = (props) => {
    return (<div>
        <div className='headerContainer'>
            <img src={logo} className='headerLogo' alt="logo" />
            <span>
                {props.title}
            </span>
        </div>

    </div>)
}