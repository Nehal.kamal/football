import { Breadcrumb, Collapse, Icon } from 'antd';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { get_all_leagues, get_teams, setSelectedCompID } from '../store/actions/index';
import { List } from './list';
import './componentsStyle.css'

const { Panel } = Collapse;
class Competitions extends Component {
    state = {
        redirect: false
    }

    componentDidMount() {
        this.get_all_leaguesHandler();

    }

    get_all_leaguesHandler = () => {
        this.props.onget_all_leagues();
    };

    getTeams(id) {
        this.props.onget_teams(id);
    }

    setRedirect = (id) => {
        this.props.onsetSelectedCompID(id)
        this.setState({
            redirect: true
        })
    }
    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to={'/league/' + this.props.selectedLeague} />
        }
    }
    render() {
        return (
            <div className='compContainer'>
                {this.props.loading && <Icon type="loading" className='loadingIcon' />
                }
                <Breadcrumb>
                    <Breadcrumb.Item>Home</Breadcrumb.Item>
                </Breadcrumb>
                <div className='compContent'>

                    {this.renderRedirect()}

                    {this.props.leaguesAreas.map(item => {
                        return (
                            <Collapse key={item} expandIconPosition='right'>
                                <Panel header={item} key={item} className='leftAlign' extra={this.props.leaguesGropedByArea[item].length}>

                                    {
                                        this.props.leaguesGropedByArea[item].map(nestitem => {
                                            return (
                                                <List key={nestitem.id} data={nestitem} showBtn={true} setRedirect={this.setRedirect} />
                                            )
                                        })
                                    }


                                </Panel>
                            </Collapse>
                        )
                    })}
                   
                </div>
            </div>

        )
    }
}

const mapStateToProps = state => {
    return {
        leagues: state.leagues.leagues,
        leaguesGropedByArea: state.leagues.leaguesGropedByArea,
        loading: state.leagues.loading,
        leaguesAreas: state.leagues.leaguesAreas,
        selectedLeague: state.leagues.selectedLeague
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onget_all_leagues: () => dispatch(get_all_leagues()),
        onget_teams: (id) => dispatch(get_teams(id)),
        onsetSelectedCompID: (id) => dispatch(setSelectedCompID(id))

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Competitions);
