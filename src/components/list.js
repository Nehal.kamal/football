import { Avatar, Button } from 'antd';
import React from 'react';
import logo from '../assets/logo.jpg';
import './componentsStyle.css'

export const List = (props) => {
    return (
        <div className='listContainer'>
            <Avatar src={props.TeamFlag ? props.data.crestUrl : props.data.emblemUrl || logo} />
            <div className='listItem'>
                <h3>
                    {props.data.name}
                </h3>
                <h5>
                    {props.data.currentSeason && 'Start Date: ' + props.data.currentSeason.startDate}
                </h5>
                <h5>
                    {props.data.currentSeason && 'End Date: ' + props.data.currentSeason.endDate}
                </h5>
            </div>
            {props.showBtn && <Button onClick={props.setRedirect.bind(this, props.data.id)}>
                Details
</Button >}
        </div>
    )
}