import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { App } from './App';
import { Header } from './components/header';
import { Footer } from './components/footer'
import TeamDetails from './components/teamDetails';
import Teams from './components/teams';
import './index.css';
import * as serviceWorker from './serviceWorker';
import configreStore from './store/configureStore';
const store = configreStore()
const ReactRedux = (
    <Provider store={store}>
        <Header title='Football Leagues' />
        <Router>
            <Switch>
                <Route exact path="/" component={App} />
                <Route exact path="/league/:id?" component={Teams} />
                <Route exact path="/league/:id?/:teamId?" component={TeamDetails} />
            </Switch>
        </Router>
        <Footer />
    </Provider>)

ReactDOM.render(ReactRedux, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
