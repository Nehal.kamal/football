import { GetALLLEAGUES, GetLEAGUESTEAMS, GETTEAMDETAILS, Loading, ErrorApi, SelectedCompID, GETLEAGUEDETAILS, SelectTeam } from './actionTypes'
import axios from 'axios';

const api = axios.create({
    baseURL: 'http://api.football-data.org/v2/'
});
api.defaults.headers.common['X-Auth-Token'] = 'c587d247404c494b87bf73f1dcd93444';

export const get_all_leagues = () => {

    return (dispatch) => {
        dispatch({ type: Loading })
        api.get('competitions').then(res => {
            dispatch({ type: GetALLLEAGUES, payload: res.data })
        }, err => {
            dispatch({ type: ErrorApi })
        })
    }
}

export const get_teams = (id) => {

    return (dispatch) => {
        dispatch({ type: Loading })
        api.get('competitions/' + id + '/teams').then(res => {
            dispatch({ type: GetLEAGUESTEAMS, payload: res.data })
        }, err => {
            dispatch({ type: ErrorApi })
        })
    }

}

export const setSelectedCompID = (id) => {
    return {
        type: SelectedCompID,
        payload: id
    }
}

export const getCompDetails = (id) => {
    return (dispatch) => {
        dispatch({ type: Loading })
        api.get('competitions/' + id).then(res => {
            dispatch({ type: GETLEAGUEDETAILS, payload: res.data })
        }, err => {
            dispatch({ type: ErrorApi })
        })
    }
}

export const get_team_details = (id) => {
    return (dispatch) => {
        dispatch({ type: Loading })
        api.get('teams/' + id).then(res => {
            dispatch({ type: GETTEAMDETAILS, payload: res.data })
        }, err => {
            dispatch({ type: ErrorApi })
        })
    }

}


export const setSelectedTeam = (team) => {
    debugger
    return {
        type: SelectTeam,
        payload: team
    }
}