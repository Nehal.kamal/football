export const GetALLLEAGUES = 'GET_ALL_LEAGUES';
export const GETLEAGUEDETAILS = 'GET_LEAGUE_DETAILS';
export const GetLEAGUESTEAMS = 'GET_LEAGUES_TEAMS';
export const GETTEAMDETAILS = 'GET_TEAM_DETAILS';
export const Loading = 'LOADING';
export const ErrorApi = 'ERROR_API';
export const SelectedCompID = 'SET_SELECED_COMP_ID';
export const SelectTeam = 'SELECT_TEAM'