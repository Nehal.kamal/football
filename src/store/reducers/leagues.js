import _ from 'lodash';
import { ErrorApi, GetALLLEAGUES, GETLEAGUEDETAILS, GetLEAGUESTEAMS, GETTEAMDETAILS, Loading, SelectedCompID } from '../actions/actionTypes';

const initialState = {
    loading: false,
    leagues: [],
    teams: [],
    selectedLeague: null,
    selectedTeamDetails: null,
    leaguesGropedByArea: [],
    leaguesAreas: []
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case Loading:
            return {
                ...state,
                loading: true,
            }
        case ErrorApi:
            return {
                ...state,
                loading: false
            }
        case GetALLLEAGUES:
            return {
                ...state,
                leagues: action.payload.competitions,
                leaguesGropedByArea: _.groupBy(action.payload.competitions, 'area.name'),
                leaguesAreas: _.keys(_.groupBy(action.payload.competitions, 'area.name')),
                loading: false
            };
        case SelectedCompID:
            return {
                ...state,
                selectedLeague: action.payload
            }
        case GETLEAGUEDETAILS:
            return {
                ...state,
                loading: false,
                selectedLeague: action.payload
            }
        case GetLEAGUESTEAMS:
            return {
                ...state,
                teams: action.payload.teams,
                loading: false
            }
        case GETTEAMDETAILS:
            return {
                ...state,
                selectedTeamDetails: action.payload,
                loading: false
            }

        default:
            return state;
    }
}

export default reducer;