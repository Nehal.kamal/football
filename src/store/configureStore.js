import { createStore, combineReducers, applyMiddleware  } from 'redux';
import leaguesReducer from './reducers/leagues'
import thunk from 'redux-thunk';


const rootReducer = combineReducers({
    leagues: leaguesReducer
})

const configreStore = () => {
    return createStore(rootReducer,  applyMiddleware(thunk))
}


export default configreStore;